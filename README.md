# README

## Generierung eines Word-Dokument

Augrund der Vermischung von Markdown und HTML in der Datei Empfehlungen_Dateiformate_HeFDI.md empfielt sich die Erstellung eines PDF mittels [pandoc](https://pandoc.org/) in zwei Schritte über das Zwischenformat HTML.

Für die Erstellung im Corporate Design der Landesiniative kann die entsprechende Designvorlage verwendet werden: (**plan.io Link/Aufgabennummer**).

Schritt 1:
```powershell
pandoc -i Empfehlungen_Dateiformate_HeFDI.md --template HeFDI_Template.docx -o Empfehlungen_Dateiformate_HeFDI.html
```

Schritt 2a: Word-Datei
```powershell
pandoc -o Empfehlungen_Dateiformate_HeFDI.html --template HeFDI_Template.docx -o Empfehlungen_Dateiformate_HeFDI.docx
```

Für ein besseres Ergebnis werden folgende "händische" Nachbearbeitungen mit Microsoft Word 2019 empfohlen:

* Die Tabelle mit der Übersicht über die Dateiformate mit Rahmenlinie versehen
* Vor und nach der Tabelle Absatzumbrüche einfügen und die Tabelle ins Querformat setzen.

Ein PDF/A-konformes Dokument kann dann über die Exportfunktion von Microsoft Word 2019 erzeugt werden.

